<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    /**
     * An event is represented by an ID, a location, a date, a code
    */
    protected $fillable = [
        'location',
        'date',
        'code',
    ];
}
