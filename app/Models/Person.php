<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    /**
     * A person is represented by an ID, a name, a surname, a status (enum), an address, a city, a postal code, a phone number, an email and a comment
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'status',
        'address',
        'city',
        'postal_code',
        'phone',
        'email',
        'comment',
    ];

    /**
     * A person has a status
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * A person has many items
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function getFullNameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }
}
