<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    /**
     * A category is represented by an ID, a name
     */
    protected $fillable = [
        'name',
    ];

    /**
     * A category has many items
    */
    public function items()
    {
        return $this->hasMany(Item::class);
    }
}
