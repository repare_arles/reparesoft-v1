<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    /**
     * A status is represented by an ID and a name
     */
    protected $fillable = [
        'name',
    ];

    /**
     * A status has many persons
     */
    public function persons()
    {
        return $this->hasMany(Person::class);
    }
}
