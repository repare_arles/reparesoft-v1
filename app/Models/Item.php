<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    /**
     * An item is made up of an ID, a person ID, a category ID, a short name, a long name, a constructor, a serial number, a weight, a value, a link to a picture, a timeslot, a boolean to say if the person came and a boolean to say if the item is in the shop (this will be modified in the future to be a foreign key to a shop table).
     */
    protected $fillable = [
        'person_id',
        'category_id',
        'short_name',
        'long_name',
        'constructor',
        'serial_number',
        'weight',
        'value',
        'picture',
        'timeslot',
        'came',
        'in_shop',
    ];

    /**
     * An item belongs to a person.
     */
    public function owner()
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

    /**
     * An item belongs to a category.
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
