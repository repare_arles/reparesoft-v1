<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

use App\Models\Category;
use App\Models\Event;
use App\Models\Person;
use App\Models\Item;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard' , [
        'categories' => Category::all(),
        'events' => Event::all(),
        'people' => Person::all(),
        'items' => Item::all(),
    ]);
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    
    // Category
    // category.show route
    Route::get('/category/{category}', function (Category $category) {
        return view('category.show', ['category' => $category]);
    })->name('category.show');

    // Event
    // event.show route
    Route::get('/event/{event}', function (Event $event) {
        return view('event.show', ['event' => $event]);
    })->name('event.show');

    // Item
    // item.show route
    Route::get('/item/{item}', function (Item $item) {
        return view('item.show', ['item' => $item]);
    })->name('item.show');

    // Person
    // person.show route
    Route::get('/person/{person}', function (Person $person) {
        return view('person.show', ['person' => $person]);
    })->name('person.show');
});

require __DIR__.'/auth.php';
