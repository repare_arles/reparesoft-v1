<x-app-layout>
<x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ $item->short_name }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            
            @if ($item->timeslot)
            <p class="text-gray-900 dark:text-gray-100">
                Réservation prévue sur le créneau {{ $item->timeslot }}
            </p>
            <br>
            @endif

            @if (!$item->came)
            <p class="text-red-500 dark:text-red-400">
                Aucune mention de la venue de l'objet
            </p>
            <br>
            @endif

            @if ($item->in_shop)
            <p class="text-green-500 dark:text-green-400">
                L'objet est actuellement en atelier
            </p>
            <br>
            @endif

            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <ul class="list-disc px-6">
                        <li>
                            <strong>Nom court:</strong> {{ $item->short_name }}
                        </li>
                        <li>
                            <strong>Nom long:</strong> {{ $item->long_name }}
                        </li>
                        <li>
                            <strong>Propriétaire :</strong> 
                            <a href="{{ route('person.show', $item->owner->id) }}" class="underline">
                                {{ $item->owner->getFullNameAttribute() }}
                            </a>
                        </li>
                        <li>
                            <strong>Catégorie :</strong> 
                            <a href="{{ route('category.show', $item->category->id) }}" class="underline">
                                {{ $item->category->name }}
                            </a>
                        </li>
                        <li>
                            <strong>Poids :</strong> {{ $item->weight }} kg
                        </li>
                        <li>
                            <strong>Valeur :</strong> {{ $item->value }} €
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <br>

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <ul class="list-disc px-6">
                        <li>
                            <strong>N° de série:</strong> {{ $item->serial_number }}
                        </li>
                        <li>
                            <strong>Constructeur:</strong> {{ $item->constructor }}
                        </li>
                        <li>
                            <strong>Propriétaire :</strong> 
                            <a href="{{ route('person.show', $item->owner->id) }}" class="underline">
                                {{ $item->owner->getFullNameAttribute() }}
                            </a>
                        </li>
                        <li>
                            <strong>Catégorie :</strong> 
                            <a href="{{ route('category.show', $item->category->id) }}" class="underline">
                                {{ $item->category->name }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
    </div>
</x-app-layout>
