<div class="sticky top-0 bg-white dark:bg-gray-800 overflow-hidden shadow-sm p-4">
    <nav class="">
        <ul class="gap-2">
            <li>
                <a href="" class="group flex items-center px-2 py-2 text-gray-900 dark:text-gray-100 rounded-md hover:bg-gray-50 dark:hover:bg-gray-700">
                    Catégories
                </a>
            </li>
            <li>
                <a href="" class="group flex items-center px-2 py-2 text-gray-900 dark:text-gray-100 rounded-md hover:bg-gray-50 dark:hover:bg-gray-700">
                    Evénements
                </a>
            </li>
            <li>
                <a href="" class="group flex items-center px-2 py-2 text-gray-900 dark:text-gray-100 rounded-md hover:bg-gray-50 dark:hover:bg-gray-700">
                    Objets
                </a>
            </li>
            <li>
                <a href="" class="group flex items-center px-2 py-2 text-gray-900 dark:text-gray-100 rounded-md hover:bg-gray-50 dark:hover:bg-gray-700">
                    Personnes
                </a>
            </li>
        </ul>
    </nav>
</div>