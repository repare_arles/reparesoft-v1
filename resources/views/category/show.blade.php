<x-app-layout>
<x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Categorie') }} {{ $category->name }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <p>Statistiques de la catégorie {{ $category->name }}</p>
                    <ul class="list-disc px-6">
                        <li>
                            <strong>Nombre d'objets :</strong> {{ $category->items->count() }}
                        </li>
                        <li>
                            <strong>Poids total :</strong> {{ $category->items->sum('weight') }} kg
                        </li>
                        <li>
                            <strong>Valeur totale :</strong> {{ $category->items->sum('value') }} €
                        </li>
                    </ul>
                </div>
            </div>

            <br>

            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <p>Proportions des objets dans la catégorie {{ $category->name }} : {{ number_format($category->items->count() / \App\Models\Item::count() * 100, 2) }} %</p>                    
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
