<x-app-layout class="h-dvh">
    
    <div class="flex w-dvw h-screen">
        <x-sidebar></x-sidebar> 

        <div class="block w-screen">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm p-4 w-full">
                <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                    {{ __('RépareSoft') }}
                </h2>
            </div>

            <div class="block p-6">
            <div class="text-center p-2 rounded bg-white dark:bg-gray-800 dark:text-gray-100">
                    <p>Bienvenue sur RépareSoft, le site de gestion de réparations de matériel.</p>
                    <p>Commencez par rechercher une personne, un objet ou un événement.</p>
                </div>
            </div>

        </div>
    </div>

    <!--
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    {{ __("You're logged in!") }}
                </div>
            </div>
        </div>


        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <p>Rechercher une personne</p>
                    <input type="text" placeholder="Search" class="border border-gray-300 rounded-md px-4 py-2 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:border-transparent dark:bg-gray-700 dark:text-gray-100">
                    <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Rechercher</button>
                </div>
            </div>
            <div class="results"></div>
        </div>

        <br>

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    Catégories
                    <ul class="list-disc px-6">
                        @foreach($categories as $category)
                            <li>
                                <a href="{{ route('category.show', $category->id) }}" class="underline">
                                    {{ $category->name }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <br>

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    Événements
                    <ul class="list-disc px-6">
                        @foreach($events as $event)
                            <li>
                                <a href="{{ route('event.show', $event->id) }}" class="underline">
                                {{ $event->location }} - {{ $event->date }} - {{ $event->code }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <br>

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    Objets
                    <ul class="list-disc px-6">
                        @foreach($items as $item)
                            <li>
                                <a href="{{ route('item.show', $item->id) }}" class="underline">
                                {{ $item->short_name }} - appartenant à {{ $item->owner->getFullNameAttribute() }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        
    </div>
-->
</x-app-layout>
