<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categories = [
            ['name' => 'électroménager'],
            ['name' => 'jardinage'],
            ['name' => 'luminaire'],
            ['name' => 'jouet'],
            ['name' => 'tissu'],
            ['name' => 'hifi'],
            ['name' => 'electronique'],
            ['name' => 'informatique'],
            ['name' => 'objet'],
            ['name' => 'vêtement'],
        ];

        foreach ($categories as $category) {
            $category = new Category($category);
            $category->save();
        }

        $this->command->info('Categories created');            
    }
}
