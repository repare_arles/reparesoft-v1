<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Person;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $persons = [
            [
                'first_name' => 'John', 
                'last_name' => 'Doe', 
                'email' => 'test@example.com',
                'status_id' => 1,
                'address' => '123 Main St',
                'city' => 'Anytown',
                'postal_code' => '12345',
                'phone' => '555-1234',
                'comment' => 'This is a test person',
            ],
            [
                'first_name' => 'Jane', 
                'last_name' => 'Smith', 
                'email' => 'test2@example.com',
                'status_id' => 2,
                'address' => '456 Elm St',
                'city' => 'Othertown',
                'postal_code' => '54321',
                'phone' => '555-4321',
                'comment' => 'This is another test person',
            ]
        ];

        foreach ($persons as $p) {
            $person = new Person();
            $person->first_name = $p['first_name'];
            $person->last_name = $p['last_name'];
            $person->email = $p['email'];
            $person->status_id = $p['status_id'];
            $person->address = $p['address'];
            $person->city = $p['city'];
            $person->postal_code = $p['postal_code'];
            $person->phone = $p['phone'];
            $person->comment = $p['comment'];
            $person->save();
        }
    }
}
