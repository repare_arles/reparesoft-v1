<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $status = [
            ['name' => 'adhérent'],
            ['name' => 'réparateur'],
            ['name' => 'bénévole'],
            ['name' => 'partenaire'],
            ['name' => 'autre'],
        ];

        foreach ($status as $s) {
            $status = new Status();
            $status->name = $s['name'];
            $status->save();
        }
    }
}
