<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Item;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            [
                'person_id' => 1,
                'category_id' => 1,
                'short_name' => 'Item 1',
                'long_name' => 'Item 1 long name',
                'constructor' => 'Item 1 constructor',
                'serial_number' => 'Item 1 serial number',
                'weight' => 1.1,
                'value' => 1.1,
                'picture' => 'Item 1 picture',
                'timeslot' => 'Item 1 timeslot',
                'came' => true,
                'in_shop' => true,
            ],
            [
                'person_id' => 2,
                'category_id' => 2,
                'short_name' => 'Item 2',
                'long_name' => 'Item 2 long name',
                'constructor' => 'Item 2 constructor',
                'serial_number' => 'Item 2 serial number',
                'weight' => 2.2,
                'value' => 2.2,
                'picture' => 'Item 2 picture',
                'timeslot' => 'Item 2 timeslot',
                'came' => false,
                'in_shop' => false,
            ],
            [
                'person_id' => 2,
                'category_id' => 3,
                'short_name' => 'Item 3',
                'long_name' => 'Item 3 long name',
                'constructor' => 'Item 3 constructor',
                'serial_number' => 'Item 3 serial number',
                'weight' => 3.3,
                'value' => 3.3,
                'picture' => 'Item 3 picture',
                'timeslot' => 'Item 3 timeslot',
                'came' => true,
                'in_shop' => true,
            ],
        ];

        foreach ($items as $item) {
            $i = new Item();
            $i->person_id = $item['person_id'];
            $i->category_id = $item['category_id'];
            $i->short_name = $item['short_name'];
            $i->long_name = $item['long_name'];
            $i->constructor = $item['constructor'];
            $i->serial_number = $item['serial_number'];
            $i->weight = $item['weight'];
            $i->value = $item['value'];
            $i->picture = $item['picture'];
            $i->timeslot = $item['timeslot'];
            $i->came = $item['came'];
            $i->in_shop = $item['in_shop'];
            
            $i->save();
        }
    }
}
