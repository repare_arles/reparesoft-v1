<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('person_id')->constrained();
            $table->foreignId('category_id')->constrained();
            $table->string('short_name');
            $table->string('long_name');
            $table->string('constructor');
            $table->string('serial_number');
            $table->float('weight');
            $table->float('value');
            $table->string('picture');
            $table->string('timeslot');
            $table->boolean('came');
            $table->boolean('in_shop');

            $table->index('person_id');
            $table->index('category_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('items');
    }
};
